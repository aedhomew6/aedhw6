package Interface.Manager;

import java.awt.CardLayout;
import javax.swing.JPanel;

public class AddProductJPanel extends javax.swing.JPanel {
    private JPanel userContainerProcess;
    private JPanel managerProcessContainer;

    
    public AddProductJPanel(JPanel userContainerProcess,JPanel managerProcessContainer) {
        initComponents();
         this.userContainerProcess=userContainerProcess;
        this.managerProcessContainer= managerProcessContainer;
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblManager = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtProductrName = new javax.swing.JTextField();
        brnAddSupplier = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();

        lblManager.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblManager.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblManager.setText("New Product");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Product Name:");

        txtProductrName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtProductrName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtProductrNameActionPerformed(evt);
            }
        });

        brnAddSupplier.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        brnAddSupplier.setText("Add Product");
        brnAddSupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                brnAddSupplierActionPerformed(evt);
            }
        });

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnBack.setText("<<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(117, 117, 117)
                        .addComponent(lblManager, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(104, 104, 104)
                        .addComponent(jLabel2)
                        .addGap(41, 41, 41)
                        .addComponent(txtProductrName, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(119, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(brnAddSupplier)
                .addGap(107, 107, 107))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblManager, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtProductrName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(113, 113, 113)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(121, 121, 121)
                        .addComponent(brnAddSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(224, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtProductrNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtProductrNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtProductrNameActionPerformed

    private void brnAddSupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_brnAddSupplierActionPerformed
        AddProductJPanel panel = new AddProductJPanel(userContainerProcess,managerProcessContainer);
        userContainerProcess.add("AddProductJPanel",panel);
        CardLayout layout = (CardLayout)userContainerProcess.getLayout();
        layout.next(userContainerProcess);

    }//GEN-LAST:event_brnAddSupplierActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userContainerProcess.remove(this);
        CardLayout layout = (CardLayout) userContainerProcess.getLayout();
        layout.previous(userContainerProcess);
    }//GEN-LAST:event_btnBackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton brnAddSupplier;
    private javax.swing.JButton btnBack;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblManager;
    private javax.swing.JTextField txtProductrName;
    // End of variables declaration//GEN-END:variables
}

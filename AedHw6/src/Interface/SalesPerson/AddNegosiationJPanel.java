package Interface.SalesPerson;

import java.awt.CardLayout;
import javax.swing.JPanel;

public class AddNegosiationJPanel extends javax.swing.JPanel {

    private JPanel userContainerProcess;
    private JPanel salesProcessContainer;
		
    
    public AddNegosiationJPanel(JPanel userContainerProcess,JPanel salesProcessContainer) {
        initComponents();
        this.userContainerProcess= userContainerProcess;
        this.salesProcessContainer= salesProcessContainer;
                }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        splitPane1 = new javax.swing.JSplitPane();
        controlOffer = new javax.swing.JPanel();
        btnOffer1 = new javax.swing.JButton();
        btnOffer2 = new javax.swing.JButton();
        btnOffer3 = new javax.swing.JButton();
        offerContainer = new javax.swing.JPanel();

        btnOffer1.setText("Offer 1");
        btnOffer1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOffer1ActionPerformed(evt);
            }
        });

        btnOffer2.setText("Offer 2");

        btnOffer3.setText("Offer 3");

        javax.swing.GroupLayout controlOfferLayout = new javax.swing.GroupLayout(controlOffer);
        controlOffer.setLayout(controlOfferLayout);
        controlOfferLayout.setHorizontalGroup(
            controlOfferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(controlOfferLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(controlOfferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnOffer1)
                    .addComponent(btnOffer2)
                    .addComponent(btnOffer3))
                .addContainerGap(23, Short.MAX_VALUE))
        );
        controlOfferLayout.setVerticalGroup(
            controlOfferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(controlOfferLayout.createSequentialGroup()
                .addGap(76, 76, 76)
                .addComponent(btnOffer1)
                .addGap(53, 53, 53)
                .addComponent(btnOffer2)
                .addGap(62, 62, 62)
                .addComponent(btnOffer3)
                .addContainerGap(238, Short.MAX_VALUE))
        );

        splitPane1.setLeftComponent(controlOffer);

        javax.swing.GroupLayout offerContainerLayout = new javax.swing.GroupLayout(offerContainer);
        offerContainer.setLayout(offerContainerLayout);
        offerContainerLayout.setHorizontalGroup(
            offerContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 393, Short.MAX_VALUE)
        );
        offerContainerLayout.setVerticalGroup(
            offerContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 498, Short.MAX_VALUE)
        );

        splitPane1.setRightComponent(offerContainer);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(splitPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(splitPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnOffer1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOffer1ActionPerformed
        Offer1JPanel panel = new Offer1JPanel(userContainerProcess,offerContainer);
        userContainerProcess.add("SalesPersonLoginJPanel",panel);
        CardLayout layout = (CardLayout)userContainerProcess.getLayout();
        layout.next(userContainerProcess);





    }//GEN-LAST:event_btnOffer1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOffer1;
    private javax.swing.JButton btnOffer2;
    private javax.swing.JButton btnOffer3;
    private javax.swing.JPanel controlOffer;
    private javax.swing.JPanel offerContainer;
    private javax.swing.JSplitPane splitPane1;
    // End of variables declaration//GEN-END:variables
}

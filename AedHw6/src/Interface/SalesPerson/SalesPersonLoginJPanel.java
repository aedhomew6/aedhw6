package Interface.SalesPerson;

import java.awt.CardLayout;
import javax.swing.JPanel;

public class SalesPersonLoginJPanel extends javax.swing.JPanel {

    private JPanel userContainerProcess;
    public SalesPersonLoginJPanel(JPanel userContainerProcess ) {
        initComponents();
        this.userContainerProcess=userContainerProcess;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        splitPane1 = new javax.swing.JSplitPane();
        controlSalesPerson = new javax.swing.JPanel();
        btnLogout = new javax.swing.JButton();
        btnCustomerOrder = new javax.swing.JButton();
        btnAllocation = new javax.swing.JButton();
        btnDashBoard = new javax.swing.JButton();
        salesProcessContainer = new javax.swing.JPanel();

        btnLogout.setText("Log out");

        btnCustomerOrder.setText("Customer Order");
        btnCustomerOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomerOrderActionPerformed(evt);
            }
        });

        btnAllocation.setText("Allocation");
        btnAllocation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAllocationActionPerformed(evt);
            }
        });

        btnDashBoard.setText("Dash Board");

        javax.swing.GroupLayout controlSalesPersonLayout = new javax.swing.GroupLayout(controlSalesPerson);
        controlSalesPerson.setLayout(controlSalesPersonLayout);
        controlSalesPersonLayout.setHorizontalGroup(
            controlSalesPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
            .addGroup(controlSalesPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(controlSalesPersonLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(controlSalesPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btnCustomerOrder, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                        .addComponent(btnLogout, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                        .addComponent(btnDashBoard, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                        .addComponent(btnAllocation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap(39, Short.MAX_VALUE)))
        );
        controlSalesPersonLayout.setVerticalGroup(
            controlSalesPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 498, Short.MAX_VALUE)
            .addGroup(controlSalesPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(controlSalesPersonLayout.createSequentialGroup()
                    .addGap(143, 143, 143)
                    .addComponent(btnLogout)
                    .addGap(38, 38, 38)
                    .addComponent(btnCustomerOrder)
                    .addGap(44, 44, 44)
                    .addComponent(btnAllocation)
                    .addGap(38, 38, 38)
                    .addComponent(btnDashBoard)
                    .addContainerGap(143, Short.MAX_VALUE)))
        );

        splitPane1.setLeftComponent(controlSalesPerson);

        salesProcessContainer.setLayout(new java.awt.CardLayout());
        splitPane1.setRightComponent(salesProcessContainer);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(splitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(splitPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCustomerOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCustomerOrderActionPerformed
        

        

    }//GEN-LAST:event_btnCustomerOrderActionPerformed

    private void btnAllocationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAllocationActionPerformed
        AddNegosiationJPanel panel = new AddNegosiationJPanel(userContainerProcess,salesProcessContainer);
        userContainerProcess.add("SalesPersonLoginJPanel",panel);
        CardLayout layout = (CardLayout)userContainerProcess.getLayout();
        layout.next(userContainerProcess);

    }//GEN-LAST:event_btnAllocationActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAllocation;
    private javax.swing.JButton btnCustomerOrder;
    private javax.swing.JButton btnDashBoard;
    private javax.swing.JButton btnLogout;
    private javax.swing.JPanel controlSalesPerson;
    private javax.swing.JPanel salesProcessContainer;
    private javax.swing.JSplitPane splitPane1;
    // End of variables declaration//GEN-END:variables
}

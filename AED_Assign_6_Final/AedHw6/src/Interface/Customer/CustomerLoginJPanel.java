package Interface.Customer;

import java.awt.CardLayout;
import javax.swing.JPanel;
import Business.*;
import Interface.UserLogin.LoginJPanel;
import java.awt.Component;
public class CustomerLoginJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private CustomerDirectory cd;
    private Customer c;
    private MasterOrderCatalog moc;
    private SupplierDirectory sd;
    private MarketList ml;
    private MarketOfferCatalog mofc;
    
    public CustomerLoginJPanel(JPanel userProcessContainer,Customer c,SupplierDirectory sd, MasterOrderCatalog moc,MarketList ml,MarketOfferCatalog mofc ) {
        initComponents();
        this.userProcessContainer= userProcessContainer;
        this.c= c;
        this.sd= sd;
        this.moc= moc;
        this.ml= ml;
        this.mofc = mofc;
}

    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        splitPane1 = new javax.swing.JSplitPane();
        controlCustomerJPanel = new javax.swing.JPanel();
        btnLogout = new javax.swing.JButton();
        btnViewDetails = new javax.swing.JButton();
        btnSearchProduct = new javax.swing.JButton();
        btnOrderHistory = new javax.swing.JButton();
        lblCustomerNameBanner = new javax.swing.JLabel();
        lblCustomerName = new javax.swing.JLabel();
        customerProcessContainer = new javax.swing.JPanel();

        btnLogout.setText("Log out");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        btnViewDetails.setText("View / Update Details");
        btnViewDetails.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewDetailsActionPerformed(evt);
            }
        });

        btnSearchProduct.setText("Search Product");
        btnSearchProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchProductActionPerformed(evt);
            }
        });

        btnOrderHistory.setText("Order History");
        btnOrderHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrderHistoryActionPerformed(evt);
            }
        });

        lblCustomerNameBanner.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblCustomerNameBanner.setText("Customer Name :");

        lblCustomerName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout controlCustomerJPanelLayout = new javax.swing.GroupLayout(controlCustomerJPanel);
        controlCustomerJPanel.setLayout(controlCustomerJPanelLayout);
        controlCustomerJPanelLayout.setHorizontalGroup(
            controlCustomerJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(controlCustomerJPanelLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(controlCustomerJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(controlCustomerJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(btnOrderHistory, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnViewDetails)
                        .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSearchProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(controlCustomerJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(lblCustomerName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblCustomerNameBanner, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)))
                .addContainerGap(45, Short.MAX_VALUE))
        );
        controlCustomerJPanelLayout.setVerticalGroup(
            controlCustomerJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(controlCustomerJPanelLayout.createSequentialGroup()
                .addComponent(lblCustomerNameBanner, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblCustomerName, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(btnLogout)
                .addGap(37, 37, 37)
                .addComponent(btnViewDetails)
                .addGap(37, 37, 37)
                .addComponent(btnSearchProduct)
                .addGap(28, 28, 28)
                .addComponent(btnOrderHistory)
                .addContainerGap(224, Short.MAX_VALUE))
        );

        splitPane1.setTopComponent(controlCustomerJPanel);

        javax.swing.GroupLayout customerProcessContainerLayout = new javax.swing.GroupLayout(customerProcessContainer);
        customerProcessContainer.setLayout(customerProcessContainerLayout);
        customerProcessContainerLayout.setHorizontalGroup(
            customerProcessContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 393, Short.MAX_VALUE)
        );
        customerProcessContainerLayout.setVerticalGroup(
            customerProcessContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );

        splitPane1.setRightComponent(customerProcessContainer);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(splitPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(splitPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewDetailsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewDetailsActionPerformed
        CustomerViewUpdateProfileJPanel panel = new CustomerViewUpdateProfileJPanel(userProcessContainer,customerProcessContainer,c,ml);
        userProcessContainer.add("CustomerViewUpdateProfileJPanel",panel);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        
        
        
    }//GEN-LAST:event_btnViewDetailsActionPerformed

    private void btnSearchProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchProductActionPerformed
        CustomerSearchProductJPanel panel = new CustomerSearchProductJPanel(userProcessContainer,customerProcessContainer,c,sd,moc,mofc);
        userProcessContainer.add("CustomerSearchProductJPanel",panel);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);

    }//GEN-LAST:event_btnSearchProductActionPerformed

    private void btnOrderHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrderHistoryActionPerformed
        CustomerOrderHistoryJPanel panel = new CustomerOrderHistoryJPanel(userProcessContainer,customerProcessContainer,c,sd,moc);
        userProcessContainer.add("CustomerOrderHistoryJPanel",panel);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);

    }//GEN-LAST:event_btnOrderHistoryActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        userProcessContainer.remove(this);
        Component[] ca = userProcessContainer.getComponents();
        Component c = ca[ca.length-1];
        LoginJPanel hp = (LoginJPanel) c; 
        hp.refresh();
        hp.test();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);

        // TODO add your handling code here:
    }//GEN-LAST:event_btnLogoutActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnOrderHistory;
    private javax.swing.JButton btnSearchProduct;
    private javax.swing.JButton btnViewDetails;
    private javax.swing.JPanel controlCustomerJPanel;
    private javax.swing.JPanel customerProcessContainer;
    private javax.swing.JLabel lblCustomerName;
    private javax.swing.JLabel lblCustomerNameBanner;
    private javax.swing.JSplitPane splitPane1;
    // End of variables declaration//GEN-END:variables
}

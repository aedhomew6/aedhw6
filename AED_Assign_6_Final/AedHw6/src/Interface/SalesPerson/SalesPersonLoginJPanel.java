package Interface.SalesPerson;

import Business.*;
import Interface.UserLogin.LoginJPanel;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JPanel;

public class SalesPersonLoginJPanel extends javax.swing.JPanel {

    private JPanel userContainerProcess;
    private MasterOrderCatalog moc;
    private Employee e;
    
    public SalesPersonLoginJPanel(JPanel userContainerProcess, Employee e, MasterOrderCatalog moc) {
        initComponents();
        this.userContainerProcess = userContainerProcess;
        this.moc = moc;
        this.e = e;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        splitPane1 = new javax.swing.JSplitPane();
        controlSalesPerson = new javax.swing.JPanel();
        btnLogout = new javax.swing.JButton();
        btnServeCustomer = new javax.swing.JButton();
        btnAllocation = new javax.swing.JButton();
        btnDashBoard = new javax.swing.JButton();
        salesProcessContainer = new javax.swing.JPanel();

        btnLogout.setText("Log out");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        btnServeCustomer.setText("Serve Customer");
        btnServeCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnServeCustomerActionPerformed(evt);
            }
        });

        btnAllocation.setText("Allocation");
        btnAllocation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAllocationActionPerformed(evt);
            }
        });

        btnDashBoard.setText("Dash Board");

        javax.swing.GroupLayout controlSalesPersonLayout = new javax.swing.GroupLayout(controlSalesPerson);
        controlSalesPerson.setLayout(controlSalesPersonLayout);
        controlSalesPersonLayout.setHorizontalGroup(
            controlSalesPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 184, Short.MAX_VALUE)
            .addGroup(controlSalesPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(controlSalesPersonLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(controlSalesPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btnServeCustomer, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                        .addComponent(btnLogout, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                        .addComponent(btnDashBoard, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                        .addComponent(btnAllocation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap(39, Short.MAX_VALUE)))
        );
        controlSalesPersonLayout.setVerticalGroup(
            controlSalesPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 498, Short.MAX_VALUE)
            .addGroup(controlSalesPersonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(controlSalesPersonLayout.createSequentialGroup()
                    .addGap(143, 143, 143)
                    .addComponent(btnLogout)
                    .addGap(38, 38, 38)
                    .addComponent(btnServeCustomer)
                    .addGap(44, 44, 44)
                    .addComponent(btnAllocation)
                    .addGap(38, 38, 38)
                    .addComponent(btnDashBoard)
                    .addContainerGap(143, Short.MAX_VALUE)))
        );

        splitPane1.setLeftComponent(controlSalesPerson);

        salesProcessContainer.setLayout(new java.awt.CardLayout());
        splitPane1.setRightComponent(salesProcessContainer);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(splitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(splitPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnServeCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnServeCustomerActionPerformed
        ServeCustomerJPanel panel = new ServeCustomerJPanel(userContainerProcess,salesProcessContainer, e,moc);
        userContainerProcess.add("ServeCustomerJPanel",panel);
        CardLayout layout = (CardLayout)userContainerProcess.getLayout();
        layout.next(userContainerProcess);
    }//GEN-LAST:event_btnServeCustomerActionPerformed

    private void btnAllocationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAllocationActionPerformed
        AddNegotiationJPanel panel = new AddNegotiationJPanel(userContainerProcess,salesProcessContainer);
        userContainerProcess.add("AddNegotiationJPanel",panel);
        CardLayout layout = (CardLayout)userContainerProcess.getLayout();
        layout.next(userContainerProcess);

    }//GEN-LAST:event_btnAllocationActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        userContainerProcess.remove(this);
        Component[] ca = userContainerProcess.getComponents();
        Component c = ca[ca.length-1];
        LoginJPanel hp = (LoginJPanel) c; 
        hp.refresh();
        hp.test();
        CardLayout layout = (CardLayout) userContainerProcess.getLayout();
        layout.previous(userContainerProcess);



        // TODO add your handling code here:
    }//GEN-LAST:event_btnLogoutActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAllocation;
    private javax.swing.JButton btnDashBoard;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnServeCustomer;
    private javax.swing.JPanel controlSalesPerson;
    private javax.swing.JPanel salesProcessContainer;
    private javax.swing.JSplitPane splitPane1;
    // End of variables declaration//GEN-END:variables
}

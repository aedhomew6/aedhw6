/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Manager;

import Business.Market;
import Business.MarketList;
import Business.Supplier;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author pramo
 */
public class AddMarket extends javax.swing.JPanel {

    /**
     * Creates new form ManageMarket
     */
    private JPanel userProcessContainer;
    private JPanel managerProcessContainer;
    private MarketList mL;
    
    public AddMarket(JPanel userProcesscontainer,JPanel managerProcessContainer, MarketList mL) {
        initComponents();
        this.userProcessContainer=userProcesscontainer;
        this.managerProcessContainer= managerProcessContainer;
        this.mL = mL;
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtMarketName = new javax.swing.JTextField();
        brnAddMarket = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        lblManager = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        txtMarketName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtMarketName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMarketNameActionPerformed(evt);
            }
        });

        brnAddMarket.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        brnAddMarket.setText("Add Market");
        brnAddMarket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                brnAddMarketActionPerformed(evt);
            }
        });

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnBack.setText("<<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        lblManager.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblManager.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblManager.setText("New Market");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Market");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 597, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(162, 162, 162)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(22, 22, 22)
                            .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(39, 39, 39)
                            .addComponent(brnAddMarket))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(jLabel2)
                            .addGap(48, 48, 48)
                            .addComponent(txtMarketName, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(lblManager, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(163, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(137, 137, 137)
                    .addComponent(lblManager, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(31, 31, 31)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtMarketName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2))
                    .addGap(88, 88, 88)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(brnAddMarket, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(137, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtMarketNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMarketNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMarketNameActionPerformed

    private void brnAddMarketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_brnAddMarketActionPerformed
    if(txtMarketName.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Kindly enter Market Name.");
        }
        else {
            Market m = mL.addMarket();
            m.setTypeOfMarket(txtMarketName.getText());
        }
    txtMarketName.setText("");
    for(Market m : mL.getMarketList()){
        System.out.println(m);
    }
    }//GEN-LAST:event_brnAddMarketActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.remove(this);
        Component[] ca = userProcessContainer.getComponents();
        Component c = ca[ca.length-1];
        ManageSupplierJPanel hp = (ManageSupplierJPanel) c; 
        hp.loadJPanel();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton brnAddMarket;
    private javax.swing.JButton btnBack;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblManager;
    private javax.swing.JTextField txtMarketName;
    // End of variables declaration//GEN-END:variables
}

package Interface.Manager;

import Business.Market;
import Business.MarketList;
import Business.Supplier;
import Business.SupplierDirectory;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

public class ManageSupplierJPanel extends javax.swing.JPanel {

    private JPanel userProcesscontainer;
    private JPanel managerProcessContainer;
    private SupplierDirectory supplierDirectory; 
    private MarketList marketList;
		
    public ManageSupplierJPanel(JPanel userProcessContainer, JPanel manageProcessContainer, SupplierDirectory supplierDirectory, MarketList marketList) {
        initComponents();
        this.userProcesscontainer=userProcessContainer;
        this.managerProcessContainer= manageProcessContainer;
        this.marketList = marketList;
        this.supplierDirectory = supplierDirectory;
        
        loadJPanel();
    }
    
    public void loadJPanel(){
        
        
        DefaultTableModel dtm = (DefaultTableModel) tbleSupplier.getModel();
        dtm.setRowCount(0);
        for(Supplier s : supplierDirectory.getSupplierList()){
            Object row[] = new Object[1];
            row[0] = s;
            dtm.addRow(row);
        }
        
        DefaultTableModel dtmm = (DefaultTableModel) tbleMarket.getModel();
        dtmm.setRowCount(0);
        for(Market m : marketList.getMarketList()){
            Object row1[] = new Object[1];
            row1[0] = m;
            dtmm.addRow(row1);
            System.out.println(m);
        }
    }
    
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnBack = new javax.swing.JButton();
        btnRemoveSupplier = new javax.swing.JButton();
        btnAddSupplier = new javax.swing.JButton();
        btnRemoveMarket = new javax.swing.JButton();
        btnAddMarket = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbleMarket = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbleSupplier = new javax.swing.JTable();

        btnBack.setText("<<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnRemoveSupplier.setText("Remove");
        btnRemoveSupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveSupplierActionPerformed(evt);
            }
        });

        btnAddSupplier.setText("Add Supplier");
        btnAddSupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddSupplierActionPerformed(evt);
            }
        });

        btnRemoveMarket.setText("Remove");
        btnRemoveMarket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveMarketActionPerformed(evt);
            }
        });

        btnAddMarket.setText("Add Market");
        btnAddMarket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddMarketActionPerformed(evt);
            }
        });

        tbleMarket.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Market Name"
            }
        ));
        jScrollPane1.setViewportView(tbleMarket);

        tbleSupplier.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Supplier Name"
            }
        ));
        jScrollPane2.setViewportView(tbleSupplier);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addGap(217, 217, 217)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnRemoveSupplier)
                        .addGap(18, 18, 18)
                        .addComponent(btnAddSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRemoveMarket)
                        .addGap(18, 18, 18)
                        .addComponent(btnAddMarket, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(75, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(79, 79, 79)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(353, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnRemoveSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnAddSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnRemoveMarket, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnAddMarket, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(71, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(78, 78, 78)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(163, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcesscontainer.remove(this);
        CardLayout layout = (CardLayout) userProcesscontainer.getLayout();
        layout.previous(userProcesscontainer);

    }//GEN-LAST:event_btnBackActionPerformed

    private void btnRemoveSupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveSupplierActionPerformed
        if(tbleSupplier.getSelectedRow()>=0){
        Supplier s = (Supplier) tbleSupplier.getValueAt(tbleSupplier.getSelectedRow(), 0);
        supplierDirectory.removeSupplier(s);
        JOptionPane.showMessageDialog(null, "Supplier has been removed!");
        loadJPanel();
        }
        else
            JOptionPane.showMessageDialog(null, "Kindly select  row to delete!");

    }//GEN-LAST:event_btnRemoveSupplierActionPerformed

    private void btnAddSupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddSupplierActionPerformed

        AddSupplierJPanel panel = new AddSupplierJPanel(userProcesscontainer,managerProcessContainer,supplierDirectory);
        userProcesscontainer.add("AddSupplierJPanel",panel);
        CardLayout layout = (CardLayout)userProcesscontainer.getLayout();
        layout.next(userProcesscontainer);
        
        
    }//GEN-LAST:event_btnAddSupplierActionPerformed

    private void btnRemoveMarketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveMarketActionPerformed
        if(tbleMarket.getSelectedRow()>=0){
        Market m = (Market) tbleMarket.getValueAt(tbleMarket.getSelectedRow(), 0);
        marketList.removeMarket(m);
        JOptionPane.showMessageDialog(null, "Market has been removed!");
        loadJPanel();
        }
        else
            JOptionPane.showMessageDialog(null, "Kindly select  row to delete!");

    }//GEN-LAST:event_btnRemoveMarketActionPerformed

    private void btnAddMarketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddMarketActionPerformed
        // TODO add your handling code here:
        AddMarket panel = new AddMarket(userProcesscontainer,managerProcessContainer,marketList);
        userProcesscontainer.add("AddMarket",panel);
        CardLayout layout = (CardLayout)userProcesscontainer.getLayout();
        layout.next(userProcesscontainer);
    }//GEN-LAST:event_btnAddMarketActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddMarket;
    private javax.swing.JButton btnAddSupplier;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnRemoveMarket;
    private javax.swing.JButton btnRemoveSupplier;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tbleMarket;
    private javax.swing.JTable tbleSupplier;
    // End of variables declaration//GEN-END:variables
}

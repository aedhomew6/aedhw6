/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Manasi
 */
public class EmployeeDirectory {
     private ArrayList<Employee> employeeDirectory;
     
    public EmployeeDirectory(){
        employeeDirectory = new ArrayList<Employee>();
    }

    public ArrayList<Employee> getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(ArrayList<Employee> employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }
     
     public Employee addEmployee(){
        Employee employee = new Employee();
        employeeDirectory.add(employee);
        return employee; 
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Manasi
 */
public class Business {
    private SupplierDirectory supplierDirectory;
    private MasterOrderCatalog masterOrderCatalog;
    private MarketOfferCatalog marketOfferCatalog;
    private MarketList marketList;
    private CustomerDirectory customerDirectory;
    private EmployeeDirectory employeeDirectory;

    
    public Business(){
        supplierDirectory = new SupplierDirectory() ;
        masterOrderCatalog = new MasterOrderCatalog() ;

        marketOfferCatalog = new MarketOfferCatalog();
        marketList = new MarketList();
        customerDirectory = new CustomerDirectory();
        employeeDirectory = new EmployeeDirectory();

        marketOfferCatalog = new MarketOfferCatalog() ;
        marketList = new MarketList() ;
        customerDirectory = new CustomerDirectory() ;
        employeeDirectory = new EmployeeDirectory() ;

        
    }

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public MasterOrderCatalog getMasterOrderCatalog() {
        return masterOrderCatalog;
    }

    public void setMasterOrderCatalog(MasterOrderCatalog masterOrderCatalog) {
        this.masterOrderCatalog = masterOrderCatalog;
    }

    public MarketOfferCatalog getMarketOfferCatalog() {
        return marketOfferCatalog;
    }

    public void setMarketOfferCatalog(MarketOfferCatalog marketOfferCatalog) {
        this.marketOfferCatalog = marketOfferCatalog;
    }

    public MarketList getMarketList() {
        return marketList;
    }

    public void setMarketList(MarketList marketList) {
        this.marketList = marketList;
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(EmployeeDirectory employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Manasi
 */
public class MarketList {
    private ArrayList<Market> marketList;

    public ArrayList<Market> getMarketList() {
        return marketList;
    }
    
    public MarketList(){
        marketList = new ArrayList<Market>();
    }

    public void setMarketList(ArrayList<Market> marketList) {
        this.marketList = marketList;
    }
    
    public Market addMarket(){
        Market market = new Market();
        marketList.add(market);
        return market; 
    }
    public void removeMarket(Market m){
        marketList.remove(m);
    }
    
}

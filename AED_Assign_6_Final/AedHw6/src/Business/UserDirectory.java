/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Manasi
 */
public class UserDirectory {
    private ArrayList<User> userDirectory;

    public ArrayList<User> getUserDirectory() {
        return userDirectory;
    }

    public void setUserDirectory(ArrayList<User> userDirectory) {
        this.userDirectory = userDirectory;
    }
    
    public User addUser(){
        User user = new User();
        userDirectory.add(user);
        return user; 
    }
}
